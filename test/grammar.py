import os

from arpeggio import Parser, ParseTreeNode
from arpeggio.cleanpeg import ParserPEG
from bezoar.model import Emitter


def emit_tree(emit: Emitter, node: ParseTreeNode, indent: str = "") -> None:
    if node.rule_name:
        emit(f"{indent}{node.rule_name}: {node}")

    if hasattr(node, "__iter__"):
        nextindent = indent + "  "
        for n in node:
            emit_tree(emit, n, nextindent)


def create_grammar() -> Parser:
    filename = os.path.join(os.path.dirname(__file__), "grammar.peg")
    with open(filename, "r") as grammar_file:
        grammar_string = grammar_file.read()
    return ParserPEG(grammar_string, "sentences",
                     skipws=True,
                     reduce_tree=False,
                     autokwd=True,
                     ignore_case=True,
                     memoization=True)
