import asyncio
from typing import Optional

from arpeggio import NoMatch, ParseTreeNode
from test.grammar import create_grammar, emit_tree

from bezoar.console.console import Console


async def main_loop() -> None:
    console = Console()

    def emit(s: str) -> None:
        console.print(s)

    grammar = create_grammar()
    try:
        console.start_transcript("grammar_transcript.log")
        while True:
            command = await console.read_line("> ")
            if not command:
                continue
            command.replace(",", " , ")
            command.replace(".", " . ")
            command.replace(";", " ; ")
            command = " ".join(command.split())
            if command in ["q", "quit", "exit"]:
                emit("Bye!")
                break

            failure: Optional[str] = None

            try:
                emit(f"Received: {command}")
                tree: ParseTreeNode = grammar.parse(command)
                emit_tree(emit, tree)
            except NoMatch as e:
                failure = str(e)
            if failure:
                emit(failure)
        return 0
    finally:
        console.stop_transcript()


def run() -> int:
    asyncio.run(main_loop())
    return 0


if __name__ == '__main__':
    exit(run())
