import logging
import asyncio
from bezoar.console import key_stroke
from bezoar.console.terminal import Terminal


async def main():
    terminal = Terminal()
    while True:
        key = await terminal.read_key()
        if key is None:
            terminal.write("No Key.\n")
            continue
        terminal.write("Key: ")
        terminal.write(str(key))
        terminal.write("\n")
        if key == key_stroke.CARRIAGE_RETURN or key == key_stroke.LINE_FEED:
            terminal.write("Done.\n")
            return
        elif key == key_stroke.CANCEL:
            terminal.write("Cancel.\n")
            return


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    asyncio.run(main())
