from bezoar.model.location import find_location_data
import unittest

from bezoar import find_data, find_location_data, location, reset


class LocationTest(unittest.TestCase):
    def setUp(self):
        reset()

    def tearDown(self):
        reset()

    def test_it_all(self):
        expected_id = "a"
        expected_name = "locname"
        expected_desc = "locdesc"
        loc = location(id=expected_id,
                       name=expected_name,
                       description=expected_desc,
                       exits={})
        self.assertIsNotNone(loc)
        self.assertEqual(expected_id, loc.id)
        self.assertEqual(expected_name, loc.name)
        self.assertEqual(expected_desc, loc.description)
        self.assertEqual(loc, find_data(expected_id))
        self.assertEqual(loc, find_location_data(expected_id))

    def test_example(self):
        house_west = location(
            id="house_west",
            name="West of House",
            description="""
                You are in front of a dilapidated house to the east, rotting
                away in the middle of a forest. The few flakes of paint that
                remain suggest that it was once painted white, but the house now
                bares its deteriorating structure to the elements.

                A once boarded-up door has largely disintegrated, leaving a gap
                you think you could squeeze through.
                """,
            exits={
                "north": "house_north",
                "south": "house_south",
                "west|in": "house_living_room",
            },
        )
        self.assertIsNotNone(house_west)
        self.assertIsNotNone(house_west.id)
        self.assertIsNotNone(house_west.name)
        self.assertIsNotNone(house_west.description)
        self.assertEqual(house_west, find_data("house_west"))
        self.assertEqual(house_west, find_location_data("house_west"))


if __name__ == '__main__':
    unittest.main()
