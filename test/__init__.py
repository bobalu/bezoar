import unittest
from test import location_test
from test import player_test

def suite():
    """ This defines all the tests of a module"""
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(location_test.LocationTest))
    suite.addTest(unittest.makeSuite(player_test.PlayerTest))
    try:
        import arpeggio
        from test import grammar_test
        suite.addTest(unittest.makeSuite(grammar_test.GrammarTest))
    except:
        pass
    return suite
