import unittest
from test.grammar import create_grammar, emit_tree
from typing import Optional

from arpeggio import NoMatch, ParseTreeNode


class GrammarTest(unittest.TestCase):
    def __init__(self, methodName: str) -> None:
        super().__init__(methodName=methodName)
        self.grammar = create_grammar()

    def test_doc_cases(self):
        cases = [
            "go inside",
            "travel south",
            "south",
            "s",
            "jump",
            "look",
            "look at yourself",
            "look at self",
            "shave",
            "shave yourself",
            "shave the orangutan",
            "jump forward",
            "jump up",
            "get the lamp",
            "drink the water",
            "get lamp",
            "get floor lamp",
            "drink water",
            "drink some water",
            "drink all the water",
            "get the bottle, knife, and sack lunch",
            "get everything from the chest",
            "get all",
            "get everything green from the chest",
            "get all swords from the chest",
            "get all green swords from the chest",
            "get everything except the snake from the chest",
            "get everything from the chest, but not the snake",
            "get all but snake from chest",
            "get the water and all swords from the chest except the snake "
                "blade",
            "get the water and all swords from the chest, except the snake "
                "blade",
            "get the water and all swords except the snake blade from the "
                "chest",
            "get the water and all swords, except the snake blade, from the "
                "chest",
            "jump over the pit",
            "look at the leaflet",
            "stand in the circle",
            "stand in front of the circle",
            "throw the ball to jerry",
            "look at myself in the mirror",
            "drink some water from the green bottle",
            "record a data record on the vinyl record",
            "attack the venomous snake with the snake blade",
            "snake the plumbing snake through the drain",
            "turn on the light",
            "turn the light on",
            "get the light ball",
            "light a fire",
            "light the light light",
            "tie the rope between all the knives",
            "tie the rope between the green pit and the blue pit",
            "tie rope between all knives",
            "tie rope between knives",

            #"get water, and all the swords from the green chest, and all the "
            #"lights from the blue chest, except the snake blade and the brass "
            #"lamp, then go north",
        ]
        failure: Optional[str] = None
        try:
            for case in cases:
                print(f"> {case}")
                tree: ParseTreeNode = self.grammar.parse(case)
                emit_tree(print, tree)
        except NoMatch as e:
            failure = str(e)
        if failure:
            self.fail(failure)


if __name__ == '__main__':
    unittest.main()
