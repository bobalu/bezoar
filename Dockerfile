FROM python:3-buster

COPY . /bezoar

RUN python -m pip install arpeggio

WORKDIR /bezoar

CMD python -m test.grammar_sample
