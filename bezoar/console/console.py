import asyncio
from asyncio.exceptions import InvalidStateError
from asyncio.locks import Event
from asyncio.tasks import Task
from asyncio.queues import Queue
from typing import Callable, IO, Optional

from bezoar.console import key_stroke
from bezoar.console.terminal import Terminal

LineCallback = Callable[[str], None]


class _LineRequest(object):
    def __init__(self,
                 prompt: Optional[str] = "") -> None:
        super().__init__()
        self.prompt = prompt


_LineRequestList = list[_LineRequest]


class Console(object):
    """An input/output console with pluggable tab completion."""

    def __init__(self, terminal: Optional[Terminal] = None) -> None:
        if terminal is not None:
            self._terminal = terminal
        else:
            # Autodetect terminal, input/output
            self._terminal = Terminal()
        self._editor_buffer = ""
        self._editor_requests = _LineRequestList()
        self._editor_visible = False
        self._wake_token = False
        self._run_task: Optional[Task] = None
        self._running_event: Event = Event()
        self._running_event.set()
        self._line_queue = Queue[str]()
        self._transcript_file:Optional[IO] = None
        self._transcript_filename:Optional[str] = None

    def print(self, message: str) -> None:
        """Prints a line to the console, adding a newline if not present."""
        self._clear_editor()

        self._terminal.write(message)
        if not message.endswith("\n"):
            self._terminal.write("\n")

        if self._transcript_file:
            self._transcript_file.write(message)
            if not message.endswith("\n"):
                self._transcript_file.write("\n")

        self._draw_editor()

    def start_transcript(self, transcript_filename:str) -> None:
        self._transcript_filename = transcript_filename
        self._transcript_file = open(transcript_filename, "a")

    def stop_transcript(self) -> None:
        if not self._transcript_file:
            return
        self._transcript_file.close()
        self._transcript_file = None
        self._transcript_filename = None

    async def read_line(self,
                        prompt: Optional[str] = "") -> Optional[str]:
        """
        Initiates user input once with the given prompt. Unless read_line() is
        called, the Console will drop input.
        """

        await self._start()
        if self.is_editing:
            return None
        self._editor_requests.append(_LineRequest(prompt))
        self._draw_editor()
        return await self._line_queue.get()

    def stop(self) -> None:
        self._wake_token = True
        self._terminal.wake_read_key()

    async def _start(self) -> None:
        if self._run_task is not None:
            return
        self._wake_token = False
        self._run_task = asyncio.create_task(self._run())

    async def _run(self) -> None:
        """
        Processes input and runs the editor, if editing. Returns False if the
        run timed out, and True if it was awoken.
        """

        try:
            while True:
                if self._wake_token:
                    self._wake_token = False
                    return

                key = await self._terminal.read_key()

                # Drop keys if not editing.
                if not self.is_editing:
                    continue

                if key is None:
                    continue
                elif key == key_stroke.BACKSPACE:
                    if not self._editor_buffer:
                        continue
                    self._terminal.backspace()
                    self._editor_buffer = self._editor_buffer[:-1]
                elif (key == key_stroke.LINE_FEED or
                    key == key_stroke.CARRIAGE_RETURN):
                    self._terminal.write("\n")
                    result = self._editor_buffer
                    self._editor_buffer = ""
                    self._pop_editor_request()
                    await self._line_queue.put(result)
                    self._editor_visible = False
                    self._draw_editor()
                    continue
                else:
                    key_str = key.name
                    self._editor_buffer += key_str
                    self._terminal.write(key_str)
        finally:
            self._run_task = None

    @property
    def is_editing(self) -> bool:
        return len(self._editor_requests) > 0

    @property
    def _prompt(self) -> Optional[str]:
        if not self.is_editing:
            return None
        return self._editor_requests[0].prompt

    def _pop_editor_request(self) -> Optional[_LineRequest]:
        if not self.is_editing:
            return None
        request = self._editor_requests[0]
        self._editor_requests = self._editor_requests[1:]
        return request

    def _clear_editor(self) -> None:
        if not self.is_editing:
            return

        if not self._editor_visible:
            return

        self._terminal.clear_line()
        self._editor_visible = False

    def _draw_editor(self) -> None:
        if not self.is_editing:
            return

        if self._editor_visible:
            return

        if self._prompt:
            self._terminal.write(self._prompt)
        self._terminal.write(self._editor_buffer)
        self._editor_visible = True
