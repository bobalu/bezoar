from typing import ClassVar, Iterable, Optional


class KeyStroke(object):
    _by_names = dict[str, 'KeyStroke']()
    _by_codes = dict[int, 'KeyStroke']()

    @classmethod
    def names(cls) -> Iterable[str]:
        return KeyStroke._by_names.keys()

    @classmethod
    def codes(cls) -> Iterable[int]:
        return KeyStroke._by_codes.keys()

    @classmethod
    def all(cls) -> Iterable['KeyStroke']:
        return KeyStroke._by_names.values()

    @classmethod
    def by_name(cls, name: str) -> Optional['KeyStroke']:
        return KeyStroke._by_names.get(name)

    @classmethod
    def by_code(cls, code: int) -> Optional['KeyStroke']:
        return KeyStroke._by_codes.get(code)

    def __init__(self, name: str, code: int, pretty_name: Optional[str] = None) -> None:
        super().__init__()
        self._name = name
        self._code = code
        if pretty_name is not None:
            self._pretty_name = pretty_name
        elif self._name.isprintable():
            self._pretty_name = self._name
        else:
            raise ValueError("unprintable")

        if self._name in KeyStroke._by_names:
            raise ValueError("duplicate name")
        KeyStroke._by_names[self._name] = self

        if self._code in KeyStroke._by_codes:
            raise ValueError("duplicate code")
        KeyStroke._by_codes[self._code] = self

    @property
    def code(self) -> int:
        return self._code

    @property
    def name(self) -> str:
        return self._name

    @property
    def pretty_name(self) -> str:
        return self._pretty_name

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, KeyStroke):
            raise TypeError()
        return self.code == o.code

    def __lt__(self, o: object) -> bool:
        if not isinstance(o, KeyStroke):
            raise TypeError()
        return self.code < o.code

    def __str__(self) -> str:
        return f"{self.pretty_name}({self.code})"


# Alphabet
A = KeyStroke("a", b"a"[0])
B = KeyStroke("b", b"b"[0])
C = KeyStroke("c", b"c"[0])
D = KeyStroke("d", b"d"[0])
E = KeyStroke("e", b"e"[0])
F = KeyStroke("f", b"f"[0])
G = KeyStroke("g", b"g"[0])
H = KeyStroke("h", b"h"[0])
I = KeyStroke("i", b"i"[0])
J = KeyStroke("j", b"j"[0])
K = KeyStroke("k", b"k"[0])
L = KeyStroke("l", b"l"[0])
M = KeyStroke("m", b"m"[0])
N = KeyStroke("n", b"n"[0])
O = KeyStroke("o", b"o"[0])
P = KeyStroke("p", b"p"[0])
Q = KeyStroke("q", b"q"[0])
R = KeyStroke("r", b"r"[0])
S = KeyStroke("s", b"s"[0])
T = KeyStroke("t", b"t"[0])
U = KeyStroke("u", b"u"[0])
V = KeyStroke("v", b"v"[0])
W = KeyStroke("w", b"w"[0])
X = KeyStroke("x", b"x"[0])
Y = KeyStroke("y", b"y"[0])
Z = KeyStroke("z", b"z"[0])

# Shifted Alphabet
SHIFT_A = KeyStroke("A", b"A"[0])
SHIFT_B = KeyStroke("B", b"B"[0])
SHIFT_C = KeyStroke("C", b"C"[0])
SHIFT_D = KeyStroke("D", b"D"[0])
SHIFT_E = KeyStroke("E", b"E"[0])
SHIFT_F = KeyStroke("F", b"F"[0])
SHIFT_G = KeyStroke("G", b"G"[0])
SHIFT_H = KeyStroke("H", b"H"[0])
SHIFT_I = KeyStroke("I", b"I"[0])
SHIFT_J = KeyStroke("J", b"J"[0])
SHIFT_K = KeyStroke("K", b"K"[0])
SHIFT_L = KeyStroke("L", b"L"[0])
SHIFT_M = KeyStroke("M", b"M"[0])
SHIFT_N = KeyStroke("N", b"N"[0])
SHIFT_O = KeyStroke("O", b"O"[0])
SHIFT_P = KeyStroke("P", b"P"[0])
SHIFT_Q = KeyStroke("Q", b"Q"[0])
SHIFT_R = KeyStroke("R", b"R"[0])
SHIFT_S = KeyStroke("S", b"S"[0])
SHIFT_T = KeyStroke("T", b"T"[0])
SHIFT_U = KeyStroke("U", b"U"[0])
SHIFT_V = KeyStroke("V", b"V"[0])
SHIFT_W = KeyStroke("W", b"W"[0])
SHIFT_X = KeyStroke("X", b"X"[0])
SHIFT_Y = KeyStroke("Y", b"Y"[0])
SHIFT_Z = KeyStroke("Z", b"Z"[0])

# Punctuation
SPACE = KeyStroke(" ", b" "[0])
EXCLAMATION_POINT = KeyStroke("!", b"!"[0])
EXCLAMATION = EXCLAMATION_POINT
QUOTE = KeyStroke("\"", b"\""[0])
HASH = KeyStroke("#", b"#"[0])
DOLLAR_SIGN = KeyStroke("$", b"$"[0])
DOLLAR = DOLLAR_SIGN
PERCENT_SIGN = KeyStroke("%", b"%"[0])
PERCENT = PERCENT_SIGN
AMPERSAND = KeyStroke("&", b"&"[0])
APOSTROPHE = KeyStroke("'", b"'"[0])
OPEN_PARENTHESIS = KeyStroke("(", b"("[0])
LEFT_PARENTHESIS = OPEN_PARENTHESIS
CLOSE_PARENTHESIS = KeyStroke(")", b")"[0])
RIGHT_PARENTHESIS = CLOSE_PARENTHESIS
ASTERISK = KeyStroke("*", b"*"[0])
PLUS_SIGN = KeyStroke("+", b"+"[0])
PLUS = PLUS_SIGN
COMMA = KeyStroke(",", b","[0])
MINUS_SIGN = KeyStroke("-", b"-"[0])
MINUS = MINUS_SIGN
PERIOD = KeyStroke(".", b"."[0])
DOT = PERIOD
SLASH = KeyStroke("/", b"/"[0])
COLON = KeyStroke(":", b":"[0])
SEMICOLON = KeyStroke(";", b";"[0])
LESS_THAN = KeyStroke("<", b"<"[0])
OPEN_ANGLE_BRACKET = LESS_THAN
LEFT_ANGLE_BRACKET = LESS_THAN
EQUAL_SIGN = KeyStroke("=", b"="[0])
EQUAL = EQUAL_SIGN
GREATER_THAN = KeyStroke(">", b">"[0])
CLOSE_ANGLE_BRACKET = LESS_THAN
RIGHT_ANGLE_BRACKET = LESS_THAN
QUESTION_MARK = KeyStroke("?", b"?"[0])
QUESTION = QUESTION_MARK
COMMERCIAL_AT = KeyStroke("@", b"@"[0])
AT_SIGN = COMMERCIAL_AT
AT = COMMERCIAL_AT
OPEN_BRACKET = KeyStroke("[", b"["[0])
LEFT_BRACKET = OPEN_BRACKET
BACKSLASH = KeyStroke("\\", b"\\"[0])
CLOSE_BRACKET = KeyStroke("]", b"]"[0])
RIGHT_BRACKET = CLOSE_BRACKET
CARAT = KeyStroke("^", b"^"[0])
UNDERSCORE = KeyStroke("_", b"_"[0])
UNDERBAR = UNDERSCORE
BACKTICK = KeyStroke("`", b"`"[0])
OPEN_BRACE = KeyStroke("{", b"{"[0])
LEFT_BRACE = OPEN_BRACE
PIPE = KeyStroke("|", b"|"[0])
CLOSE_BRACE = KeyStroke("}", b"}"[0])
RIGHT_BRACE = CLOSE_BRACE
TILDE = KeyStroke("~", b"~"[0])

# Digits
DIGIT_0 = KeyStroke("0", b"0"[0])
DIGIT_1 = KeyStroke("1", b"1"[0])
DIGIT_2 = KeyStroke("2", b"2"[0])
DIGIT_3 = KeyStroke("3", b"3"[0])
DIGIT_4 = KeyStroke("4", b"4"[0])
DIGIT_5 = KeyStroke("5", b"5"[0])
DIGIT_6 = KeyStroke("6", b"6"[0])
DIGIT_7 = KeyStroke("7", b"7"[0])
DIGIT_8 = KeyStroke("8", b"8"[0])
DIGIT_9 = KeyStroke("9", b"9"[0])

# Terminal Control Characters
NULL = KeyStroke("\x00", 0, pretty_name="NUL")
CANCEL = KeyStroke("\x03", 3, pretty_name="Cancel")
INTERRUPT = CANCEL
END_OF_TRANSMISSION = KeyStroke("\x04", 4, pretty_name="EOT")
BELL = KeyStroke("\x07", 7, pretty_name="BEL")
BACKSPACE = KeyStroke("\x08", 8, pretty_name="BS")
TAB = KeyStroke("\x09", 9, pretty_name="TAB")
LINE_FEED = KeyStroke("\x0A", 10, pretty_name="LF")
VERTICAL_TAB = KeyStroke("\x0B", 11, pretty_name="VT")
FORM_FEED = KeyStroke("\x0C", 12, pretty_name="FF")
CARRIAGE_RETURN = KeyStroke("\x0D", 13, pretty_name="CR")
ESCAPE = KeyStroke("\x1B", 27, pretty_name="ESC")
DELETE = KeyStroke("\x7F", 127, pretty_name="DEL")

# Control Alphabet
CTRL_A = KeyStroke("\x01", 1, pretty_name="^A")
CTRL_B = KeyStroke("\x02", 2, pretty_name="^B")
CTRL_C = CANCEL
CTRL_D = END_OF_TRANSMISSION
CTRL_E = KeyStroke("\x05", 5, pretty_name="^E")
CTRL_F = KeyStroke("\x06", 6, pretty_name="^F")
CTRL_G = BELL
CTRL_H = BACKSPACE
CTRL_I = TAB
CTRL_J = LINE_FEED
CTRL_K = VERTICAL_TAB
CTRL_L = FORM_FEED
CTRL_M = CARRIAGE_RETURN
CTRL_N = KeyStroke("\x0E", 14, pretty_name="^N")
CTRL_O = KeyStroke("\x0F", 15, pretty_name="^O")
CTRL_P = KeyStroke("\x10", 16, pretty_name="^P")
CTRL_Q = KeyStroke("\x11", 17, pretty_name="^Q")
CTRL_R = KeyStroke("\x12", 18, pretty_name="^R")
CTRL_S = KeyStroke("\x13", 19, pretty_name="^S")
CTRL_T = KeyStroke("\x14", 20, pretty_name="^T")
CTRL_U = KeyStroke("\x15", 21, pretty_name="^U")
CTRL_V = KeyStroke("\x16", 22, pretty_name="^V")
CTRL_W = KeyStroke("\x17", 23, pretty_name="^W")
CTRL_X = KeyStroke("\x18", 24, pretty_name="^X")
CTRL_Y = KeyStroke("\x19", 25, pretty_name="^Y")
CTRL_Z = KeyStroke("\x1A", 26, pretty_name="^Z")

# Extra Special Characaters
PREVIOUS = KeyStroke("\xC1", 0xC1, pretty_name="Previous")
PAGE_UP = PREVIOUS
NEXT = KeyStroke("\xC2", 0xC2, pretty_name="Next")
PAGE_DOWN = NEXT
END = KeyStroke("\xC3", 0xC3, pretty_name="End")
HOME = KeyStroke("\xC4", 0xC4, pretty_name="Home")
LEFT = KeyStroke("\xC5", 0xC5, pretty_name="Left")
RIGHT = KeyStroke("\xC6", 0xC6, pretty_name="Right")
UP = KeyStroke("\xC7", 0xC7, pretty_name="Up")
DOWN = KeyStroke("\xC8", 0xC8, pretty_name="Down")
INSERT = KeyStroke("\xC9", 0xC9, pretty_name="Insert")
