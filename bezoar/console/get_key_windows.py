import msvcrt
import asyncio
from typing import Optional

from bezoar.console import key_stroke
from bezoar.console.key_stroke import KeyStroke

_TranslationMap = dict[str, KeyStroke]


def _create_translation_table() -> _TranslationMap:
    table = _TranslationMap()
    for code in KeyStroke.codes():
        key = KeyStroke.by_code(code)
        if key:
            table[chr(code)] = key
    return table


def _create_escaped_translation_table() -> _TranslationMap:
    table = _TranslationMap()
    table[chr(71)] = key_stroke.HOME
    table[chr(72)] = key_stroke.UP
    table[chr(73)] = key_stroke.PAGE_UP
    table[chr(75)] = key_stroke.LEFT
    table[chr(77)] = key_stroke.RIGHT
    table[chr(79)] = key_stroke.END
    table[chr(80)] = key_stroke.DOWN
    table[chr(81)] = key_stroke.PAGE_DOWN
    table[chr(82)] = key_stroke.INSERT
    table[chr(83)] = key_stroke.DELETE
    return table


_TRANSLATION_TABLE = _create_translation_table()
_ESCAPED_TRANSLATION_TABLE = _create_escaped_translation_table()

# Using an event to make thread-safe for now.
_wake_token = asyncio.Event()


def wake_get_key() -> None:
    """
    Wakes any outstanding call to get_key, causing it to return None
    immediately.
    """

    _wake_token.set()


async def get_key() -> Optional[KeyStroke]:
    """
    Wait for a key to be hit, returning the associated KeyStroke. Returns None
    if woken up before a key was pressed.
    """

    while True:
        if _wake_token.is_set():
            _wake_token.clear()
            return None

        if not msvcrt.kbhit():  # type: ignore
            # Sleep between polling, give the CPU a break.
            await asyncio.sleep(0.005)
            continue

        escaped = False
        code = msvcrt.getwch()  # type: ignore
        if code == "\u0003":
            raise KeyboardInterrupt
        while code in "\u0000\u00E0":
            escaped = True
            code = msvcrt.getwch()  # type: ignore

        if escaped:
            key = _ESCAPED_TRANSLATION_TABLE.get(code)
        else:
            key = _TRANSLATION_TABLE.get(code)

        if key is None:
            continue

        _wake_token.clear()
        return key
