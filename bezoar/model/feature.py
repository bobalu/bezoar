from __future__ import annotations

from typing import Optional

from bezoar.model.entity import EntityData


class FeatureData(EntityData):
    """
    An interactive feature of a Location that can't be taken, but is otherwise
    very similar to an Item.
    """
    pass

    def __init__(
        self,
        id: str,
        name: str,
        description: str,
        contents: EntityData.Contents,
        properties: EntityData.Properties,
    ) -> None:
        """
        Initializes the feature.
        """
        super().__init__(id=id, name=name, description=description,
                         contents=contents, properties=properties)


def feature(
    id: str,
    name: str,
    description: str,
    contents: EntityData.Contents = [],
    properties: EntityData.Properties = {},
) -> FeatureData:
    return FeatureData(id=id, name=name, description=description,
                       contents=contents, properties=properties)
