from textwrap import fill


def normalize_words(s: str) -> str:
    return " ".join([word.strip() for word in s.strip().split(" ") if word])


def normalize_paragraphs(s: str) -> str:
    lines = [line.strip() for line in s.strip().splitlines()]
    for index in range(0, len(lines)):
        if not lines[index]:
            lines[index] = "\n"
    # One line per paragraph.
    lines = [normalize_words(line)
             for line in " ".join(lines).strip().splitlines()]
    # One wrapped paragraph per entry.
    paragraphs = [fill(line, width=78) for line in lines]
    return "\n\n".join(paragraphs)


def split_words(s: str) -> list[str]:
    s = normalize_words(s)
    s.replace(".", " . ")
    s.replace(";", " ; ")
    return s.split()
