"""
The Parser.

See [parser.md](parser.md).
"""

from typing import Optional

from bezoar.base.text import split_words
from bezoar.model.entity import Entity


# These are the canonical direction forms that you should refer to when
# declaring exits. Standard synonyms will automatically be applied.
_CANONICAL_DIRECTIONS = {
    "aft",
    "down",
    "east",
    "fore",
    "in",
    "north",
    "northeast",
    "northwest",
    "out",
    "port",
    "south",
    "southeast",
    "southwest",
    "starboard",
    "up",
    "west",
}


# Universal direction abbreviations or synonyms that will automatically be
# applied when typed by the user in a direction context.
_DIRECTION_SYNONYMS = {
    "d": "down",
    "e": "east",
    "enter": "in",
    "exit": "out",
    "n": "north",
    "ne": "northeast",
    "nw": "northwest",
    "s": "south",
    "se": "southeast",
    "sw": "southwest",
    "u": "up",
    "w": "west",
}


# These are verbs that can precede a direction to mean to take that exit from
# the location.
_MOVE_VERBS = {
    "go",
    "move",
    "travel",
    "walk",
}


# These are the prepositions that you should refer to when declaring action
# syntax. These were chosen by the criteria of being short, one word,
# and common in contemporary English usage.
_CANONICAL_PREPOSITIONS = {
    "about",
    "above",
    "across",
    "after",
    "around",
    "at",
    "away from",
    "before",
    "behind",
    "between",
    "from",
    "in front of",
    "in",
    "near",
    "on",
    "opposite",
    "out",
    "over",
    "to",
    "towards",
    "under",
    "with",
}


# Universal synonyms to canonical prepositions.
_PREPOSITION_SYNONYMS = {
    "across from": "opposite",
    "beside": "near",
    "close to": "near",
    "inside": "in",
    "into": "in",
    "next to": "near",
    "on top of": "on",
    "onto": "on",
    "on to": "on",
    "outside of": "out",
    "regarding": "about",
}


# Words that may indicate the beginning of a new sentence.
_SENTENCE_TERMINATORS = {
    ",",
    ";",
    ".",
    "and",
    "then",
}


# Articles that might be used with nouns to indicate specificity.
_ARTICLES = {
    "a",
    "an",
    "the",
}


# Conjunctions used to group multiple objects to act on.
_CONJUNCTIONS = {
    "and",
}


# Nouns that the parser will have to figure out what is being referred to.
_PRONOUNS = {
    "her",
    "him",
    "it",
    "them",
}


# Ways for the player to to refer to themselves. We leave it ambiguous whether
# the player should use the first or second person to refer to themselves.
_SELF = {
    "me",
    "myself",
    "self",
    "you",
    "yourself",
}

Word = str
Sentence = list[Word]


# Bits of the type of word a word could be. Some words can be used in multiple
# ways, so may have multiple bits set.
_ADJECTIVE   = (1 <<  0)
_AND         = (1 <<  1)
_EVERYTHING  = (1 <<  2)
_EXCEPT      = (1 <<  3)
_ARTICLE     = (1 <<  4)
_DIRECTION   = (1 <<  5)
_MOVE_VERB   = (1 <<  6)
_NOUN        = (1 <<  7)
_PREPOSITION = (1 <<  8)
_TERMINATOR  = (1 <<  9)
_VERB        = (1 << 10)



class ParseResult(object):
    """
    The complete results of a parse, as applied to the context of the player.
    """

    def __init__(
        self,
        verb: str,
        preposition: Optional[str],
        direct_object: Optional[Entity],
        indirect_object: Optional[Entity],
        remainder: str,
    ) -> None:
        super().__init__()
        self.verb = verb
        self.preposition = preposition
        self.direct_object = direct_object
        self.indirect_object = indirect_object
        self.remainder = remainder


def parse(submission: str) -> Optional[ParseResult]:
    words = split_words(submission)
    if words[0] in _SENTENCE_TERMINATORS:
        words = words[1:]
    # TODO: Obviously incomplete.
    return None
