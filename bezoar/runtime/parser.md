# The Parser

The parser attempts to parse imperative sentences, based heavily on the player's
current context.

In terms of sentence structure, all commands are in imperative form; meaning the
"tacit you" is always the subject.

The position of the parser is to act as an intermediary between the actual user
and the user's avatar in the game; as if one's brain had to communicate to one's
body as two entities. It ends up feeling more natural than it sounds.

## Definitions

Here are some technical definitions relevant to the domain. I had to look up the
precise meanings of a lot of these, so I wanted to make sure I had a good
reference.

### Parts of speech

- **noun** - A word or phrase referring to a person, place, thing, or idea.\
  _Examples:_
  - _"you"_ (pronoun person)
  - _"Jerry"_ (person)
  - _"home"_ (place)
  - _"table"_ (thing)
  - _"liberty"_ (idea)
- **pronoun** - A noun that indicates specificity based on context.\
  _Examples:_
  - _"you"_ (second person)
  - _"myself"_ (first person reflexive)
  - _"mine"_ (first person possessive)
  - _"him"_ (third person male object)
  - _"she"_ (third person female subject)
  - _"it"_ (third person neuter)
- **verb** - A word describing an action, experience, or condition.\
  _Examples:_
  - _"runs"_
  - _"hopping"_
  - _"take"_
  - _"fell"_
  - _"to be"_
- **adjective** - A word or phrase that describes or modifies a noun.\
  _Examples:_
  - _"gentle"_
  - _"helpful"_
  - _"small"_
- **adverb** - A word or phrase that describes or modifies a verb.\
  _Examples:_
  - _"gently"_
  - _"helpfully"_
  - _"someday"_
  - _"almost"_
  - _"fast"_
- **preposition** - A word or phrase used before a noun, pronoun, or noun phrase
  to show direction, time, place, location, spatial relationships, or to
  introduce an object.\
  _Examples:_
  - _"in"_
  - _"at"_
  - _"on"_
  - _"of"_
  - _"to"_
- **conjuction** - A word or phrase used to link words, clauses, and phrases.\
  _Examples:_
  - _"for"_
  - _"and"_
  - _"nor"_
  - _"but"_
  - _"or"_
  - _"yet"_
  - _"so"_
- **determiner** - A word or phrase used with a noun to limit (quantify) or give
  definiteness (refer).\
  _Examples:_
  - _"a"_ (article)
  - _"an"_ (article)
  - _"the"_ (article)
  - _"this"_ (demonstrative)
  - _"that"_ (demonstrative)
  - _"the other"_ (demonstrative)
  - _"some"_ (quantifier)
  - _"five"_ (definite amount)
  - _"Jerry's"_ (possessive)
  - _"his"_ (possessive with pronoun)
- **interjection** - A word used express feeling or command attention.\
  _Examples:_
  - _"darn"_
  - _"wow"_
  - _"hey"_

### More details

- **imperative mood** - A language form that gives a direct order or command.\
  _Examples:_
  - _"Give the ball to me."_
  - _"Hit the tree with the axe."_
  - _"Take the wood."_
  - _"Jump over the stump."_
  - _"Clap."_
- **imperative verb** - A verb conjugation that creates an imperative sentence.
  The imperative mood uses the zero infinitive form, which (with the exception
  of be) is the same as the second person in the present tense.\
  _Examples: "give" "hit" "take" "jump"_
- **reflexive pronoun** - A pronoun that refers to the sentence subject as the
  object.\
  _Examples: "yourself" "myself" and "itself"_
- **reflexive verb** - A verb whose subject and object refer to the same thing.\
  _Examples: "jump" "sit" "wave"_
- **adjectival preposition** - A word used with a noun to describe or clarify it
  further.\
  _Examples:_
  - _"the ball in the middle"_
  - _"the sword inside the chest"_
- **adverbal preposition** - A word used with a verb to describe or clarify it
  further.\
  _Examples:_
  - _"look around"_
  - _"drink with fervor"_

## Cases

We are thankfully only interested in the imperative mood, so this eliminates
large swaths of the English language. There are still a lot of cases to
consider, as well as some text adventure conventions that we need to preserve.

### Movement

In a text adventure, the world is split into discrete locations, and there are
discrete connections between locations, generally represented by a direction. As
these are the most commonly used commands, they have standard abbreviations, and
it is not required to use a standard imperative form.

> Go inside.

> Travel south.

This can be abbreviated to just the direction.

```none
> in
```

or

```none
> south
```

Respectively. For cardinal directions, just the first letter will do.

```none
> s
```

### Reflexive verbs with no preposition

The next simplest case is for reflexive verbs that don't have any preposition.

> Jump.

> Look.

This really means:

> (you) Jump (yourself).

> (you) Look (yourself).

We would never actually say this this way, but this is what is actually
happening from a sentence diagram perspective. The subject, "you," is also the
direct object, "yourself". Note that this is different from

> Look at yourself.

which actually means

> (you) Look (yourself) at yourself.

Presumably in a mirror... see a later case for prepositions.

Some verbs can be used reflexively or not.

> Shave.

means

> Shave (yourself).

which you can also say, and it wouldn't sound too weird. But, you can also

> Shave the orangutan.

Though, they wouldn't probably be very happy about it.

### Reflexive verbs with a preposition and no indirect object

Note that a preposition can change the meaning of a verb, so when parsing we
need to take prepositions into account.

> Jump forward.

> Jump up.

### Verbs with a direct object

This is the most common case of interacting with objects in the world.

> Get the lamp.

> Drink the water.

Adding object references now means we have to deal with articles, units,
multiple objects, adjectives, wildcards, and exceptions! Yikes. Let's break
these out into separate cases.

### Articles

Generally, we expect the user to be specific, and can complain if they aren't.

> Get a lamp.

Well, there's only one lamp here, so I guess you mean that one, but that's a
weird way to say it. Normally, we'd expect:

> Get the lamp.

But, because we are a game and not trying to be overly-prescriptive, we will
accept:

```none
> get lamp
```

### Adjectives

Each entity will may have one or more adjectives associated with it. This will
allow the parser to disambiguate ojects that have the same basic nomenclature.

Let's say from the example above there were two lamps, a floor lamp and a table
lamp. We'd have to ask for an adjective.

```none
> get lamp
Which lamp, the table lamp or the floor lamp?

> floor
The floor lamp is too awkward to carry around with you.
```

Here we can just give the user another prompt, and they can ignore the question
or they can answer it with a naked adjective.

### Non-count nouns

Non-count nouns are nouns that are not counted, like water, salt, information.
In these cases, either no article or the definite article is used, or an amount.

> Drink water.

> Drink the water.

> Drink some water.

> Drink all the water.

### Multiple objects

Especially for verbs like "get," the player will want to be able to easily get
multiple things at once.

> Get the bottle, knife, and sack lunch.

### Wildcards

When referring to multiple objects, the user wants to be able to refer to
everything at once, without actually enumerating every item. For this we need
a wildcard.

> Get everything from the chest.

In this case "everything" is the wildcard. We should support this, but
historically text adventures also supported "all," so we'll need to support
that, too.

```none
> get all
sword: Taken.
lamp: Taken.
snake: As you try to pick it up, the snake bites your arm and wriggles out of
your grasp. You have been envenomated!
```

One additional thing that Infocom didn't support was wildcards filtered by item
class or adjective, only by location.

> Get everything green from the chest.

> Get all swords from the chest.

> Get all green swords from the chest.

It's kind of a stretch goal, but it seems like we should be able to support
this. We'd need to be able to get the plural for any noun we recognize. Also,
how many green swords do you need?

### Exceptions

Once you have wildcards, of course, you want to be able to include everything,
and then exclude specific things. For this you need an exception.

> Get everything except the snake from the chest.

or

> Get everything from the chest but not the snake.

Or, more realistically...

```none
> get all but snake from chest
```

### All Together, Now

When talking about multiple objects, you can combine wildcards, specific items,
exceptions, and prepositions.

> Get the water, and all swords from the chest, except the snake blade.

So, if there are swords not in the chest, those won't be included. If there are
other things in the chest that aren't swords, those also wouldn't be included.
If there's a snake blade in the chest, it won't be included. The water can be
anywhere, inside or outside of the chest.

You can also say, equivalently:

> Get the water, and all swords except the snake blade, from the chest.

### Reflexive verbs with a preposition and an indirect object

> Jump over the pit.

> Look at the leaflet.

> Look at yourself.

Prepositions can be multiple words, and this can also change the meaning
entirely of the verb, even if some of the words are the same.

> Stand in the circle.

> Stand in front of the circle.

### Verbs with a direct object and an indirect object

This is pretty much the most complex scenario.

> Throw the ball to Jerry.

> Look at myself in the mirror.

> Drink some water from the green bottle.

### Multiple sentences

The strategy for multiple sentences is to consume the input incrementally and if
we can't consume across a sentence separator, we will leave it and try again
after we execute the first sentence.

### Words that can be multiple things

So, we know that "in" and "up" can both prepositions and directions. Let's come
up with some other combinations.

- "record" -- verb or noun\
  *"Record a data record on the vinyl record."*
- "snake" -- noun, adjective, or verb\
  *"Attack the venomous snake with the snake blade."*\
  *"Snake the plumbing snake through the drain."*
- "light" -- noun, adjective, or verb\
  *"Turn on the light."*\
  *"Get the light ball."*\
  *"Light the fire."*\
  *"Light the light light."* (Yikes)

## EBNF Grammar

My strategy is to define a grammar for what I'm trying to accept, and then write
a formal parser to do it. Luckily, I have a lot of leeway since I know the size
of the token stream is very small and in memory, so I can look ahead all I want
and so on.

**Note:** brackets mean optional, braces mean repeated 0 or more times.

```ebnf
direction = "north" | "south" | ... ;
go-verb = "go" | "move" | ... ;
go-command = [ go-verb ] direction ;

(* Verbs will be calculated by the game model, and augmented with built-in
   standard commands. *)
verb = "look" | "take" | "light" | ... ;

(* Prepositions can be exhaustively listed, and easily added, if not. *)
preposition = "in" "front" "of" | "under" | "behind" | ... ;

(* Nouns and adjectives will mostly come from the game declarations. *)
game-noun = "light" | ... ;
adjective = "light" | ... ;
plural-nouns = "lights" | ... ;

article = "a" | "an" | "the" ;
all = "all" ;
everything = "everything" ;
except = "except" | "excluding" | "but" [ "not" ];
and = "and" | "," ;
self = "self" | "myself" | "yourself" ;
noun = game-noun | self ;

sentence-terminator = "." | "then" | "and" ;
sentence-terminators = { sentence-terminators } ;

qualified-noun = [ article ] [ adjective ] noun ;

prepositional-phrase = preposition [ qualified-noun ] ;

wildcard = all [ plural-noun ] [ prepositional-phrase ]
         | everything [ prepositional-phrase ]
         ;

object-reference = qualified-noun | wildcard ;

multiple-objects = object-reference { and { and } object-reference } ;

exception-objects = except multiple-objects ;

objects-reference = multiple-objects [ exception-objects ] ;

imperative = go-command
           | verb [ objects-reference ] [ prepositional-phrase ]
           ;

sentences = imperative { sentence-terminators imperative } ;
```

To maximize the chances of understanding the user's intent, we should make sure
the parser includes all context when trying to semantically parse the sentence.
Some words are used in multiple ways, as adjectives, nouns, or verbs. You can
even verb nouns if you try hard enough. English is not friendly to computational
parsing in that way.

So, for example, if a word can be used as an adjective or a noun (e.g. "snake"
in "venomous snake" or "snake" in "snake blade") it should be preferred to
mean whatever makes sense in the context of both where it is used in the input,
as well as what objects the player can "see" from their current position.
