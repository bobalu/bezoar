import bezoar.model.entity
from bezoar.base.text import normalize_words
from bezoar.model import Emitter
from bezoar.model.initialize import initialize_model
from bezoar.model.player import find_player
from bezoar.runtime.parser import parse


def _no_emitter(x:str) -> None:
    pass


_emit: Emitter = _no_emitter


async def start(emitter: Emitter) -> None:
    initialize_model()
    global _emit
    _emit = emitter
    player = find_player()

    if not player:
        _emit("You are no one! <This indicates a bug.>")
        return
    player.look(_emit)


async def submit_command(command: str) -> None:
    player = find_player()

    if not player:
        _emit("You are no one! <This indicates a bug.>")
        return

    command = normalize_words(command)

    if command in ["l", "look", "look around"]:
        player.look(_emit)
        return

    if command in ["dump"]:
        _emit("STATIC DATA:")
        for ed in bezoar.model.entity.EntityData.ENTITY_DATA.values():
            _emit(str(ed))
        _emit("DYNAMIC DATA:")
        for entities in bezoar.model.entity.Entity.ENTITIES.values():
            for e in entities:
                _emit(str(e))
        return

    result = parse(command)
    if not result:
        _emit("I don't understand.")
        return
    _emit("I really don't understand. <This indicates a bug.>")

